<?php
// +----------------------------------------------------------------------
// | duoduojie app system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 duoduojie app All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace Ddj;
/**
 * 多多客api,暂不考虑授权api
 * @author zqs
 */
class Pdd
{
    public $options;
    public $server_url = 'https://gw-api.pinduoduo.com/api/router';
    private $client_secret;
    /**
     * 初始化
     */
    public function __construct($options=[])
    {
        $this->options = $options;
        //公共参数
        $db_config = get_db_config(true);
        $this->options['client_id'] = $db_config['tk_cfg']['pdd']['client_id'];
        $this->options['timestamp'] = time();
        
        $this->client_secret = $db_config['tk_cfg']['pdd']['client_secret'];
    }
    
    /**
     * 执行请求
     */
    public function request()
    {
        $this->options['sign'] = self::sign();
        $re =  json_decode(http_post($this->server_url, $this->options),true);
        return $re;
    }
    
    /**
     * 签名算法
     */
    private function sign()
    {
        ksort($this->options);
        $sign_str = '';
        foreach ($this->options as $k=>$v){
            $sign_str .= $k.$v;
        }
        $sign_str = $this->client_secret.$sign_str.$this->client_secret;
        return strtoupper(md5($sign_str));
    }
    
    
    
    
    
    
    
}