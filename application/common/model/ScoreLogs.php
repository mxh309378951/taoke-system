<?php

namespace app\common\model;
use think\Model;
class ScoreLogs extends Model
{
    // 关闭自动写入update_time字段
    protected $updateTime = false;
    /**
     * 添加、减少金额
     */
    public function add_money($data)
    {
        if (empty ( $data ) || empty ($data['type']) || empty($data['score']) || empty($data['uid']))
            return false;
            //变化前余额
            $before_blance = db('User')->where('uid',$data['uid'])->value('score');
            if ($before_blance===false)
                return false;
                $data['blance'] = $before_blance+$data['score'];
                $data['create_time'] = time();
                //添加
                if (db('score_logs')->insert($data))
                {
                    //更新总余额    避免并发情况下查询延迟出现错误
                    //$agent->where('id',$data['agent_id'])->setField('blance',$data['blance']);
                    db('user')->where('uid',$data['uid'])->setInc('score',$data['score']);
                }
                else {
                    return false;
                }
                return true;
    }
    public function getTypeAttr($value)
    {
        $text_arr = config('site.score_type');
        return $text_arr[$value];
    }
    
    
}