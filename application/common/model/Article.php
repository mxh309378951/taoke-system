<?php

namespace app\common\model;

use think\Model;

class Article extends Model
{
    //
    public function getCateIdAttr($value)
    {
        return db('ArticleCate')->where('id',$value)->value('name');
    }
}
