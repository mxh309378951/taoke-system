<?php

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\console\input\Argument;
use think\console\input\Option;
class Order extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('order')
             ->addArgument('name', Argument::OPTIONAL, "action")
             ->addOption('type', null, Option::VALUE_REQUIRED, 'tjp name')
             ->setDescription('订单相关命令');
        
    }

    protected function execute(Input $input, Output $output)
    {
        $name = trim($input->getArgument('name'));
        $name = $name ?: 'import';
        switch ($name){
            case 'import':
                $type = $input->getOption('type')??'';
                switch ($type){
                    case 'tb1':
                        //按创建时间导入淘宝订单
                        import_order_tb(60*5+30);
                        $output->writeln('导入订单-淘宝-创建时间');
                        break;
                    case 'tb2':
                        //按结算时间导入淘宝订单
                        import_order_tb(60*5+30,'settle_time');
                        $output->writeln('导入订单-淘宝-结算时间');
                        break;
                    case 'jd':
                        import_order_jd(date('YmdH'));//当前这个小时
                        break;
                    case 'pdd':
                        import_order_pdd(time()-60*10);//十分钟前
                        break;
                    default:
                        $output->writeln('参数无效');
                }
                break;
            case 'fanli':
                fanli();
                $output->writeln('结算佣金');
                break;
            default:
                $output->writeln('操作无效');
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
