<?php
// +----------------------------------------------------------------------
// | duoduojie app system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 duoduojie app All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\admin\controller\count;

use think\Controller;
use app\common\controller\Admin;

class Index extends Admin
{
    /**
     * 统计首页
     */
    public function index()
    {
       //未提现金额
       $count = [];
       $count['total_money'] = db('user')->sum('money');
       //$count['total_sd_money'] = db('user')->sum('sd_money');
       $date_start = input('date_start',date('Y-m-d',time()));
       $date_start = strtotime($date_start);
       $date_end = $date_start+86400;
       //收款金额
       $count['total_amount'] = db('paylogs')->where('pay_status',1)->whereTime('create_time', 'between',[$date_start,$date_end])->sum('amount');
       //打款金额
       $count['total_draw'] = db('draw')->where('status',1)->whereTime('create_time', 'between',[$date_start,$date_end])->sum('money');
       //佣金
       $count['total_rj'] = db('orders')->whereIn('order_status', '1,2')->whereTime('create_time', 'between',[$date_start,$date_end])->sum('income_price');
       //订单数
       $count['total_orders'] = db('orders')->count();
       $this->assign('date_start',$date_start);
       $this->assign('count',$count);
       return $this->fetch(); 
    }

}
