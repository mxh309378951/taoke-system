<?php
// +----------------------------------------------------------------------
// | llpt system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 llpt All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\common\controller\Admin;
use think\facade\Cache;
/**
* 后台设置控制器
*/
class Set extends Admin
{
    public function initialize()
    {
        parent::initialize();
    }
    /**
     * 基础设置
     */
    public function index()
    {
        
        return $this->fetch();
    }
    
    /**
     * 更改设置
     */
    public function edit()
    {
        if (IS_AJAX)
        {
            $config = input('post.config/a');
            foreach ($config as $name => $value)
            {
                //如果是数组则json_encode存入
                if (is_array($value)) $value = json_encode($value);
                model('Config')->where('name',$name)->update(['value'=>$value,'update_time'=>time()]);
            }
            //清除短信配置缓存 
            cache('__ALISMS_CONFIG__',null);
            //清除数据库配置缓存
            cache('db_config',null);
            //清除海报缓存
            Cache::clear('haibao');
            $this->success('修改成功');
        }
    }
    /**
     * 操作日志
     */
    public function logs()
    {
    	if (IS_AJAX)
    	{
    		//搜索
    		$map = [];
    		$params = input('');
    		if ($params['keyword'])
    		    $map[] = ['username|title','like',"%{$params['keyword']}%"];
    		$list = model('AdminLog')
    		->where($map)
    		->order('id desc')
    		->paginate(input('limit',15));
    		return json(layui_page($list));
    	}
    	return $this->fetch();
    }
    /**
     * 黑名单
     */
    public function blacklist()
    {
    	
    	return $this->fetch();
    }
    /**
     * 邮件配置
     */
    public function email()
    {
        //测试
        if (IS_AJAX)
        {
            $email = input('email');
            if(!filter_var($email,FILTER_VALIDATE_EMAIL))
            {
                $this->error('请输入正确的邮箱');
            }
            $html = <<<TJH
            如果您收到了这封邮件，说明已经配置成功！
TJH;
            $send = send_mail($email, '您好','测试',$html);
            if ($send===true)
            {
                $this->success('发送成功');
            }
            else 
            {
                $this->error('发送失败'.$send);
            }
        }
        //dump($this->send_mail('421549604@qq.com', '小胡','调价函','这个一个测试的内容，请勿回复！'));
        
        return $this->fetch();
    }
    /**
     * 短信配置
     */
    public function sms()
    { 
        return $this->fetch();
    }
    
    
    /**
     * app设置
     */
    public function app()
    {
        $db_config = get_db_config(true);//dump($db_config);die;
        $this->assign('app_index_banner',$db_config['app_index_banner']);
        $this->assign('app_index_icon',$db_config['app_index_icon']);
        $this->assign('app_index_bk',$db_config['app_index_bk']);
        $this->assign('app_center_ad',$db_config['app_center_ad']);
        $this->assign('app_souquan_help',$db_config['app_souquan_help']);
        $this->assign('app_apple_examine',$db_config['app_apple_examine']);
        //先品库---修改为自建版块
//         $favorites = model('GoodsMamaCate')->all();
//         $this->assign('favorites',$favorites);
        
        //$this->assign('sections',get_section_names());
        //兑吧相关
//         $duiba = new \ddj\Duiba();
//         $autologin_url = $duiba->buildCreditAutoLoginRequest($db_config['app_duiba']['appkey'], $db_config['app_duiba']['appsecret'], 1, 10000);
//         $this->assign('duiba',[
//             'autologin_url' => $autologin_url,
//         ]);
        return $this->fetch();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}