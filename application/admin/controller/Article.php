<?php

namespace app\admin\controller;

use think\Controller;
use app\common\controller\Admin;
use ddj\Tree;

class Article extends Admin
{
    protected $model;
    public function initialize()
    {
        parent::initialize();
        $this->model = model('Article');
    }
    /**
     * 列表
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            $map = [
                ['status','eq',1]
            ];
            //分类搜索
            $cate_id = (int)input('cate_id',0);
            if ($cate_id){
                $sonids = db('ArticleCate')->where('pid',$cate_id)->column('id');
                $sonids[] = $cate_id;
                $map[] = ['cate_id','in',$sonids];
            }
            $list = $this->model
            ->where($map)
            ->order('update_time desc')
            ->paginate(input('limit',15));
            //$list->append(['status_text','pid_nickname']);
            //返回layui分页
            return json(layui_page($list));
        }else {
            //所有分类
            $cate_list = model('ArticleCate')->all(['status'=>1]);
            $cate_list = Tree::instance()->init($cate_list)->getTreeArray(0);
            $layui_tree = [];
            foreach ($cate_list as $k=>$v){
                $layui_tree[$k]['id'] = $v['id'];
                $layui_tree[$k]['name'] = $v['name'];
                $layui_tree[$k]['spread'] = true;
                $layui_tree[$k]['children'] = $v['childlist'];
            }
            $this->assign('layui_tree',json_encode($layui_tree,JSON_UNESCAPED_UNICODE ));
            
            return $this->fetch();
        }
        
    }
    /**
     * 添加
     */
    public function add()
    {
        return $this->edit();   
    }
    /**
     * 编辑
     */
    public function edit($id='')
    {
        if (IS_AJAX){
            $data = input('post.');
            if (empty($id)){//添加
                model('Article')->allowField(true)->save($data);
                $this->success('添加成功');
            }else {
                model('Article')->allowField(true)->save($data,['id'=>$id]);
                
                $this->success('编辑成功');
            }
        }else {
            //所有分类
            $cate_list = model('ArticleCate')->all(['status'=>1]);
            $cate_list = Tree::instance()->init($cate_list)->getTreeArray(0);
            //dump($cate_list);
            $this->assign('cate_list',$cate_list);
            
            $info = model('Article')->get($id);
            $this->assign('info',$info);
            return $this->fetch('edit');
        }
    }
   /**
    * 删除
    */
    public function del($ids='')
    {
        parent::del($ids);
    }
    
    /**
     * 分类管理
     */
    public function cate()
    {
        //取出所有父类
        $cate_list = model('ArticleCate')->all(['status'=>1]);
        $cate_list = Tree::instance()->init($cate_list)->getTreeArray(0);
        //dump($cate_list);
        $this->assign('cate_list',$cate_list);
        return $this->fetch();
    }
    /**
     * 添加分类
     */
    public function cate_add()
    {
        return $this->cate_edit();
    }
    /**
     * 编辑分类
     */
    public function cate_edit($id='')
    {
        if ($this->request->isAjax()){
            $data = input('post.');
            if (empty($id)){//添加
                model('ArticleCate')->allowField(true)->save($data);
                $this->success('添加成功');
            }else {
                model('ArticleCate')->allowField(true)->save($data,['id'=>$id]);
                
                $this->success('编辑成功');
            }
        }else {
            //取出所有父类
            $cate_list = model('ArticleCate')->all(['status'=>1,'pid'=>0]);
            $this->assign('cate_list',$cate_list);
            //dump($cate_list);
            $info = model('ArticleCate')->get($id);
            $this->assign('info',$info);
            return $this->fetch('cate_edit');
        }
    }
    /**
     * 删除分类
     */
    public function cate_del($id='')
    {
        if (empty($id)) $this->error('请选择你要操作的数据');
        //是否有子类
        $cate = model('ArticleCate');
        if ($cate->get(['pid'=>$id])){
            $this->error('分类下有子分类，不能进行删除操作');
        }
        //是否有文章
        if ($this->model->get(['cate_id'=>$id,'status'=>1])){
            $this->error('分类下有文章，不能删除');
        }
        $this->model = $cate;
        parent::del($id);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
