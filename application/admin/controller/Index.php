<?php
// +----------------------------------------------------------------------
// | duoduojie app system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 duoduojie app All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\common\controller\Admin;
use think\captcha\Captcha;
use think\Validate;
/**
 * 后台首页控制器
 */
class Index extends Admin
{
    protected $noNeedLogin = ['login', 'logout','verify'];
    protected $noNeedRight = ['index','main','verify'];
    /**
     * 首页
     */
    public function index()
    {
        //提示 小圆点
        $param = [];
        $menulist = $this->auth->getSidebar($param);
        
        unset($menulist[0]);
        $menulist = array_values($menulist);
        $this->assign('menulist', $menulist);
        return $this->fetch();
    }
    /**
     * 控制台
     * @return string
     */
    public function main()
    {
        $db_order = db('orders');
        $db_user = db('user');
        $count = [];
        //总成交额
        $count['payfee_total'] = $db_order->whereIn('order_status', '1,2')->sum('pay_price');
        //今日成交额
        $count['payfee_today'] = $db_order->whereTime('create_time', 'today')->sum('pay_price');
        //昨日订单数
        $count['order_count_yesterday'] = $db_order->removeOption()->whereIn('order_status', '1,2')->whereTime('create_time', 'yesterday')->count();
        //今日订单数
        $count['order_count_today'] = $db_order->removeOption()->whereIn('order_status', '1,2')->whereTime('create_time', 'today')->count();
        //昨日佣金
        $count['rj_yesterday'] = $db_order->removeOption()->whereIn('order_status', '1,2')->whereTime('create_time', 'yesterday')->sum('income_price');
        //今日佣金
        $count['rj_today'] = $db_order->removeOption()->whereIn('order_status', '1,2')->whereTime('create_time', 'today')->sum('income_price');
        
        //用户总量
        $count['user_total'] = $db_user->where('status','neq',-1)->count();
        //今日新增用户
        $count['user_today'] = $db_user->removeOption()->where('status','neq',-1)->whereTime('create_time', 'today')->count();
        //dump($count);
        
        //流量走势图30日
        $echarts = [];
        $today_start = strtotime(date('Y-m-d'));
        for ($i=0;$i<30;$i++){
            $time_start = $today_start-(86400*$i);
            $time_end = $time_start+86400;
            $map = [
                ['create_time','between time',[$time_start, $time_end]]
            ];
            //用户
            $date_user = model('User')->where($map)->where('status','neq',-1)->count();
            //订单
            $date_order = model('Orders')->where($map)->whereIn('order_status', '1,2')->count();
            //佣金
            $date_rj = model('Orders')->where($map)->whereIn('order_status', '1,2')->sum('income_price');
            $echarts[] = [
                'date' => date('m-d',$time_start),
                'user' => $date_user,
                'order' => $date_order,
                'rj' => $date_rj
            ];
        }
        
        //dump($echarts);
        $this->assign('count',$count);
        $this->assign('echarts',$echarts);
        return $this->fetch();
    }
    /**
     * 管理员登录
     */
    public function login()
    {
        $url = $this->request->get('url', 'index/index');
        
        if ($this->auth->isLogin()){
            $this->redirect($url);
            //$this->error('您已登录，请不要重复登录~', $url);
            return;
        }
        if ($this->request->isPost()){
            $username = $this->request->post('username');
            $password = $this->request->post('password');
            $verifycode = $this->request->post('verifycode');
            $keeplogin = $this->request->post('keeplogin');
            $token = $this->request->post('__token__');
            $rule = [
                'username'  => 'require|length:2,30',
                'password'  => 'require|length:6,30',
                'verifycode'=> 'require|captcha',
                '__token__' => 'token',
            ];
            $data = [
                'username'  => $username,
                'password'  => $password,
                'verifycode'=> $verifycode,
                '__token__' => $token,
            ];
            $msg = [
                'username.require' => '请输入用户名',
                'username.length'  => '用户名长度在2-30个字符之间',
                'password.require' => '请输入密码',
                'password.length'  => '密码长度在6-30个字符之间',
                'verifycode.require' => '请输入验证码',
                'verifycode.captcha' => '验证码不正确',
            ];
            $validate = new Validate($rule,$msg);
            $result = $validate->check($data);
            if (!$result){
                $this->error($validate->getError(), $url, ['token' => $this->request->token()]);
                return;
            }
            $result = $this->auth->login($username, $password, $keeplogin ? 7*86400 : 0);
            if ($result === true){
                $this->success('登录成功', $url, ['url' => $url]);
                return;
            }else{
                $this->error('登录或密码错误', $url, ['token' => $this->request->token()]);
            }
            return;
        }
        // 根据客户端的cookie,判断是否可以自动登录
        if ($this->auth->autologin()){
            $this->redirect($url);
        }
        $this->view->assign('title', '管理登录');
        return $this->view->fetch();
    }
    
    /**
     * 注销登录
     */
    public function logout()
    {
        $this->auth->logout();
        $this->redirect('index/login');
        return;
    }
    
    /**
     * 验证码
     */
    public function verify()
    {
        $config = [
            'codeSet' => '0123456789',
            'fontSize' => 12,
            'length' =>5,
            'imageH' => 34,
            'imageW' => 108,
            'fontttf' => '5.ttf',
        ];
        $captcha = new Captcha($config);
        return $captcha->entry();
    }
}
