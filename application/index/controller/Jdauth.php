<?php

namespace app\index\controller;

use think\Controller;

class Jdauth extends Controller
{
    public function index()
    {
        $db_config = get_db_config(true);
        $param = [
            'response_type' => 'code',
            'client_id' => $db_config['tk_cfg']['jd']['appkey'],
            'redirect_uri' => config('site.domain').'/index/jdauth/jd_notify',
            //'state' => 'test',
            //'scope' => 'read',
            //'view' => ''
        ];
        $url = 'https://oauth.jd.com/oauth/authorize?'.http_build_query($param);
        $this->redirect($url);
    }
    public function jd_notify(){
        $code = input('code','');
        if ($code){
            $db_config = get_db_config(true);
            $param = [
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => config('site.domain').'/index/jdauth/jd_notify',
                'client_id' => $db_config['tk_cfg']['jd']['appkey'],
                'client_secret' => $db_config['tk_cfg']['jd']['appsecret'],
                'state' => 'succ',
            ];
            $url = 'https://oauth.jd.com/oauth/token';
            $re = http_post($url,$param);//trace($re);
            $re = json_decode($re,true);
            echo 'token为：'.$re['access_token'].'&nbsp;&nbsp;请复制填入';
        }
    }
    
}
