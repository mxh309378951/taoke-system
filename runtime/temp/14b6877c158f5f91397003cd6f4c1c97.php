<?php /*a:2:{s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/main.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
<style>
.info-box {
    height: 85px;
    background-color: white;
    background-color: #ecf0f5;
}

.info-box .info-box-icon {
    border-top-left-radius: 2px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 2px;
    display: block;
    float: left;
    height: 85px;
    width: 85px;
    text-align: center;
    font-size: 45px;
    line-height: 85px;
    background: rgba(0, 0, 0, 0.2);
}

.info-box .info-box-content {
    padding: 5px 10px;
    margin-left: 85px;
}

.info-box .info-box-content .info-box-text {
    display: block;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    text-transform: uppercase;
}

.info-box .info-box-content .info-box-number {
    display: block;
    font-weight: bold;
    font-size: 18px;
}

.major {
    font-weight: 10px;
    color: #01AAED;
}

.main {
    margin-top: 15px;
}

.main .layui-row {
    margin: 10px 0;
}
.row2 .info-box p{text-align: center;}
.row2 .info-box p:first-child{font-size: 20px;font-weight: bold;padding-top:20px;}
</style>

</head>

<body>
	<div class="admin-body">
		
<div class="main">
	<div class="layui-row layui-col-space15">
	    <div class="layui-col-md3">
	        <div class="info-box">
	            <span class="info-box-icon" style="background-color:#00c0ef !important;color:white;"><i class="fa fa-jpy" aria-hidden="true"></i></span>
	            <div class="info-box-content">
	                <span class="info-box-text">总成交额</span>
	                <span class="info-box-number">￥<?php echo htmlentities($count['payfee_total']); ?></span>
	            </div>
	        </div>
	    </div>
	    <div class="layui-col-md3">
	        <div class="info-box">
	            <span class="info-box-icon" style="background-color:#dd4b39 !important;color:white;"><i class="fa fa-money" aria-hidden="true"></i></span>
	            <div class="info-box-content">
	                <span class="info-box-text">今日成交</span>
	                <span class="info-box-number">￥<?php echo htmlentities($count['payfee_today']); ?></span>
	            </div>
	        </div>
	    </div>
	    <div class="layui-col-md3">
	        <div class="info-box">
	            <span class="info-box-icon" style="background-color:#00a65a !important;color:white;"><i class="fa fa-users" aria-hidden="true"></i></span>
	            <div class="info-box-content">
	                <span class="info-box-text">用户量</span>
	                <span class="info-box-number"><?php echo htmlentities($count['user_total']); ?></span>
	            </div>
	        </div>
	    </div>
	    <div class="layui-col-md3">
	        <div class="info-box">
	            <span class="info-box-icon" style="background-color:#f39c12 !important;color:white;"><i class="fa fa-user-plus" aria-hidden="true"></i></span>
	            <div class="info-box-content">
	                <span class="info-box-text">今日新增用户</span>
	                <span class="info-box-number"><?php echo htmlentities($count['user_today']); ?></span>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="layui-row layui-col-space15 row2">
		<div class="layui-col-md3">
			<div class="info-box">
				<p><?php echo htmlentities($count['order_count_yesterday']); ?></p>
				<p>昨日订单数</p>
			</div>
		</div>
		<div class="layui-col-md3">
			<div class="info-box">
				<p><?php echo htmlentities($count['order_count_today']); ?></p>
				<p>今日订单数</p>
			</div>
		</div>
		<div class="layui-col-md3">
			<div class="info-box">
				<p>￥<?php echo htmlentities($count['rj_yesterday']); ?></p>
				<p>昨日佣金</p>
			</div>
		</div>
		<div class="layui-col-md3">
			<div class="info-box">
				<p>￥<?php echo htmlentities($count['rj_today']); ?></p>
				<p>今日佣金</p>
			</div>
		</div>
		<div class="layui-col-md12">
			<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
			<div id="main" style="height:400px;"></div>
	    </div>
	</div>
</div>


	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script src="/static/echarts/echarts.min.js"></script>
<script src="/static/echarts/macarons.js"></script>
<script type="text/javascript">
	// 基于准备好的dom，初始化echarts实例
	var myChart = echarts.init(document.getElementById('main'),'macarons');
	myChart.resize({
		//width:document.body.scrollWidth-100
	});
	// 指定图表的配置项和数据
	var option = {
	    title: {
	        text: '30日流量走势'
	    },
	    toolbox: {
	        show: true,
	        feature: {
	            dataView: {readOnly: false},
	            magicType: {type: ['line', 'bar']},
	            restore: {},
	            saveAsImage: {}
	        }
	    },
	    tooltip: {
	        trigger: 'axis'
	    },
	    legend: {
	        data:['用户','订单','佣金']
	    },
	    grid: {
	        left: '3%',
	        right: '4%',
	        bottom: '3%',
	        containLabel: true
	    },
	    xAxis: {
	        type: 'category',
	        boundaryGap: false,
	        data: [<?php if(is_array($echarts) || $echarts instanceof \think\Collection || $echarts instanceof \think\Paginator): $i = 0; $__LIST__ = $echarts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>'<?php echo htmlentities($vo['date']); ?>',<?php endforeach; endif; else: echo "" ;endif; ?>]
	    },
	    yAxis: {
	        type: 'value'
	    },
	    series: [
	        {
	            name:'用户',
	            type:'line',
	            //stack: '1',
	            data:[<?php if(is_array($echarts) || $echarts instanceof \think\Collection || $echarts instanceof \think\Paginator): $i = 0; $__LIST__ = $echarts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><?php echo htmlentities($vo['user']); ?>,<?php endforeach; endif; else: echo "" ;endif; ?>],
	            markPoint:{data:[{name:"最高",type:"max"}]},
	            markLine: {
	                data: [
	                    {type: 'average', name: '平均值'}
	                ]
	            }
	        },
	        {
	            name:'订单',
	            type:'line',
	            //stack: '1',
	            data:[<?php if(is_array($echarts) || $echarts instanceof \think\Collection || $echarts instanceof \think\Paginator): $i = 0; $__LIST__ = $echarts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><?php echo htmlentities($vo['order']); ?>,<?php endforeach; endif; else: echo "" ;endif; ?>],
	            markPoint:{data:[{name:"最高",type:"max"}]},
	            markLine: {
	                data: [
	                    {type: 'average', name: '平均值'}
	                ]
	            }
	        },
	        {
	            name:'佣金',
	            type:'line',
	            //stack: '1',
	            data:[<?php if(is_array($echarts) || $echarts instanceof \think\Collection || $echarts instanceof \think\Paginator): $i = 0; $__LIST__ = $echarts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><?php echo htmlentities($vo['rj']); ?>,<?php endforeach; endif; else: echo "" ;endif; ?>],
	            markPoint:{data:[{name:"最高",type:"max"}]},
	            markLine: {
	                data: [
	                    {type: 'average', name: '平均值'}
	                ]
	            }
	        },
	    ]
	};
	// 使用刚指定的配置项和数据显示图表。
	myChart.setOption(option);
</script>

</html>