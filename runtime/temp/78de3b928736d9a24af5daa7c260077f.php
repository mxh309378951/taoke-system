<?php /*a:2:{s:70:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/set/logs.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
<style type="text/css">
.layui-select,.layui-textarea,.layui-input{height: 32px;line-height: 32px;}
</style>

</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<form class="layui-form" action="" >
		<div class="layui-inline">
			<div class="layui-input-inline">
				<input type="text" name="keyword"  placeholder="账号/事件" autocomplete="off" class="layui-input">
			</div>
		</div>
		 <div class="layui-inline">
		 	<div class="layui-input-inline">
		 		<button class="layui-btn layui-btn-sm" lay-submit lay-filter="searchsub" id="search"><i class="layui-icon">&#xe615;</i> 搜索</button>
		 	</div>
		 </div>
	</form>
</blockquote>
<table id="tb1" lay-filter="_tb1"></table>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script>
	layui.use(['tool'], function() {
		var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool;
		//搜索
		form.on('submit(searchsub)',function(data){
			var fields = $(data.form).serialize();
			table.reload('tb1',{
				where:data.field
			});
			return false;
		});
		var tableobj = table.render({
			elem:'#tb1',
			url:'<?php echo url('logs'); ?>',
			limit:20,
			limits:[10,20,50,100],
			page:true,
			height:'full-100',
			//size:'sm',
			method:'get',
			cols:[[
				{title:'账号',field:'username',width:100},
				{title:'事件标题',field:'title',width:150},
				{title:'url',field:'url',width:300},
				{title:'参数内容',field:'content',width:500},
				{title:'IP',field:'ip',width:140},
				{title:'时间',field:'create_time'},
			]]
		});

	});
</script>

</html>